<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="textml;charset=UTF-8" />
    <title>Home</title>
    <link rel="stylesheet" href="../css/home.css">
    <script src="../js/vendor/jquery-3.2.1.min.js"></script>

</head>

<!--=======================================================================================================================================-->

<body>

    <!--header-->

    <div id="header">
        <div class="bg wp">
            <table id="menubar" class="wp">
                <tr>
                    <td>
                        <ul id="bar">
                            <li class="menu"  id="selected"><a href="#"><img src="../img/header/home.png" class="smlogo"> 首页</a></li>
                            <li class="menu"><a href="#" ><img src="../img/header/cd.png" class="smlogo">汉化硬盘版</a></li>
                            <li class="menu"><a href="#" ><img src="../img/header/book.png" class="smlogo">新人必读</a></li>
                            <li class="menu"><a href="#" ><img src="../img/header/zip.png" class="smlogo">解压必读</a></li>
                            <li class="menu"><a href="#" ><img src="../img/header/star.png" class="smlogo">收藏夹</a></li>
                            <li class="menu"><a href="#" ><img src="../img/header/message.png" class="smlogo">留言板</a></li>
                        </ul>
                    </td>
                    <td id="search">
                        <form action="search.php" id="search_form" name="search">
                            <input type="search" id="search_key" name="search" placeholder="What are you looking for?">
                            <button type="submit" onclick="">Search</button>
                        </form>
                    </td>
                </tr>
            </table>
        </div>
    </div>


    <!--================================================================================================================================-->

    <!--center-->
    <div id="center">
        <div id="left">
            <?php include_once '../php/gameinfo.php';   while($result=$res2->fetch(PDO::FETCH_ASSOC)){
                ?>
            <div class="main_out">
                <div class="main border">
                    <h1 class="title"><?php echo $result['name'];?></h1>
                    <span class="labels">
                        <a class="label" ><?php echo $result['id']; ?></a>
                        <a class="label"><?php echo $result['name']; ?></a>
                        <a class="label"><?php echo $result['date']; ?></a>
                    </span>
                    <a href="#" >
                        <img src="../img/testPics/003.jpg" alt="" class="main_pic">
                        <p class="main_p">
                            <?php echo $result['info']; ?>
                        </p>
                    </a>

                </div>
            </div>
            <?php
                } echo '';?>
            <!--<div class="main border">-->

            <!--</div>-->
            <div class="pages">
                <a href="../php/gameinfo.php?page=1">首页</a>
                <a href='../php/gameinfo.php?page=<?php echo $prev;?>'>上一页</a>
                <a href='../php/gameinfo.php?page=<?php echo $next;?>'>下一页</a>
                <a href='../php/gameinfo.php?page=<?php echo $pages;?>'>末页</a>
            </div>
        </div>

        <div id="right">

            <div id="log" class="right border">
                <div id="log_heading">
                    <span>请登录：</span>
                </div>
                <div>
                    <form id="log_" action="../php/login/log_in.php" method="post">
                        <table id="log_table">
                            <tr class="log_row">
                                <td>
                                    <img src="../img/log/user.png" alt="user.png" class="log_img"/>
                                    <input type="text" id="username" class="log_text" name="username" placeholder=" 用户名">
                                </td>
                            </tr>
                            <tr class="log_row">
                                <td>
                                    <img src="../img/log/password.png" alt="user.png" class="log_img">
                                    <input type="password" id="password" class="log_text" name="password" placeholder=" 密码">
                                </td>
                            </tr>
                            <tr class="log_row">
                                <td width=6%></td>
                                <td class="button_td">
                                    <input type="submit" id="log_button" class="log_buttons border" name="submit" value="登录">
                                </td>
                                <td width=2%></td>
                                <td class="button_td">
                                    <input type="button" id="reg_button" class="log_buttons border" name="regist" value="注册">
                                </td>
                            </tr>
                            <tr>
                                <td id="forget">
                                    <a href="#">忘记密码？</a>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>


        </div>
    </div>

    <!--====================================================================================================================================-->

    <!--footer-->


    <div class="clear"></div>

    <div id="footer" class="bg wp">
        <span class="bottom">this is a footer | have a happy day | : )</span>
    </div>

</body>
</html>
