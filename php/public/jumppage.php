<?php
/**
 * Created by IntelliJ IDEA.
 * User: HASEE
 * Date: 2017/12/8
 * Time: 23:31
 */
header('Content-type:text/html;charset=utf-8');

/*
 * 跳转函数
 * @param1 string $url,跳转目标
 * @param2 string $msg,提示信息
 * @param3 int $time = 3,跳转等待时间
*/
function redirect($url,$msg,$time=3){
    //跳转
    header("Refresh:{$time};url={$url}");
    //提示
    echo $msg;
    //终止脚本执行
    exit;
}