<?php
/**
 * Created by IntelliJ IDEA.
 * User: HASEE
 * Date: 2017/12/9
 * Time: 15:46
 */
header('Content-type:text/html ; charset=utf-8');

include_once 'public/jumppage.php';

if (isset($_POST['submit'])){
    $search = $_POST['search'];
    if (empty($search)){
        redirect('search.php','请输入查找信息!');
    }else {
        try {
            include_once 'public/logindb.php';
            global $pdo;
            $pdo->setAttribute(PDO::ATTR_CASE, PDO::CASE_UPPER);
            $count = 0;
            $page = 1;
            $stmt = $pdo->prepare("SELECT * FROM kwibrid.gameinfo WHERE name LIKE '%?%' ORDER BY oid  desc ");
            if ($stmt->execute(array($search))) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $rownum = $stmt->fetchAll(PDO::FETCH_ASSOC);
                echo "相关信息如下： <br/>";
                while ($row = $stmt->fetch()&& $count <=$rownum) {
                    if ($count <=8){
                        print_r($row);
                        $count++;}
                    else{
                        $page++;
                        //跳转下一个页面
                        print_r($row);
                        $count =1;
                    }
                }
            } else {
                redirect('home.php', '没有查找到相关信息');
            }
        } catch (PDOException $ex) {
            echo '出现异常：<br/>';
            echo '错误出现的位置：' . $ex->getFile() . $ex->getLine() . '<br/>';
            echo '错误原因：' . $ex->getMessage();
            var_dump($ex->getTrace());
            exit;
        }
    }
}else{
    include_once 'home.php ';
}
